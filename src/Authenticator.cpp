#include "../include/Authenticator.h"
#include <iostream>
#include "cryptopp/cryptlib.h"
#include "cryptopp/hmac.h"
#include "cryptopp/sha.h"
#include "cryptopp/base64.h"
#include "cryptopp/filters.h"

using CryptoPP::Exception;
using CryptoPP::HMAC;
using CryptoPP::SHA256;
using CryptoPP::Base64Decoder;
using CryptoPP::Base64Encoder;
using CryptoPP::StringSink;
using CryptoPP::StringSource;
using CryptoPP::HashFilter;


std::string Authenticator::Sign(std::string t_stamp, std::string request, std::string location, std::string message)
{
	std::string mac, encoded, s_key;
	std::string base = t_stamp + request + location + message;
	StringSource(secret, true, new Base64Decoder(new StringSink(s_key)));

	try
	{
			HMAC<SHA256> hmac((unsigned char*)s_key.c_str(), s_key.length());
			StringSource(base, true, new HashFilter(hmac, new StringSink(mac)));
	}
	catch(const CryptoPP::Exception& e)
	{
			std::cerr << e.what() << std::endl;
	}
	
	StringSource(mac, true, new Base64Encoder(new StringSink(encoded)));
	encoded.erase(44, 1);
	return encoded;
}

std::string Authenticator::GetTimestamp()
{
	time_t t = time(0);
	return std::to_string(t);
}

Authenticator::Authenticator(std::string key, std::string secret, std::string passphrase)
{ 
	this->key = key;
	this->secret = secret;
	this->passphrase = passphrase;
}
