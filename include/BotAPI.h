#pragma once

#include "Authenticator.h"
#include <string>

class BotAPI
{
	private:
  	std::string Call(std::string method, bool authed, std::string path, std::string body);
	public:
  	BotAPI();
  	~BotAPI();
  	Authenticator auth;
  	std::string uri;
  	std::string product_id;
  	std::string Get_Buy_Price();
  	double Get_Balance(std::string currency);
  	std::string Place_Limit_Order(std::string side, std::string price, std::string size);
};
