#pragma once
#include <string>

class Authenticator
{
		public:
				std::string key;
				std::string secret;
				std::string passphrase;
				std::string GetTimestamp();
				std::string Sign(std::string t_stamp, std::string request, std::string location, std::string message);

				Authenticator(){}
				Authenticator(std::string key, std::string secret, std::string passphrase);
};
